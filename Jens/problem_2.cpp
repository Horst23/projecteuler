#include <iostream>


int main()
{
    int f1 = 1;
    int f2 = 2;
    int f3 = 1;
    int sum = 2;
    int counter = 0;
    while(f3 <= 4000000)
    {
        f3 = f1 + f2;
        f1 = f2;
        f2 = f3;
        counter += 1;
        if(counter == 3)
        {
            sum += f3;
            counter = 0;
        }
    }
    std::cout << sum << std::endl;
}
