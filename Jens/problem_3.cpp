#include <iostream>
#include <deque> 
#include <cmath>

std::deque<int> eratosthenes(int limit)
{
    bool numberfield[limit];
    std::deque<int> primes;
    for(int i = 0; i<=limit; i++)
        numberfield[i] = true;

    for(int i = 2; i<=limit; i++)
    {
        if(numberfield[i] == true)
        {
            int j = 2;
            while(i*j <= limit)
            {
                numberfield[i*j] = false;
                j++;
            }
        }
    }
    for(int i=2; i<=limit; i++)
    {
        if(numberfield[i] == true)
            primes.push_front(i);        
    }
    return primes;
}

/*int large_prime(std::deque<int> primes, int num)
{
    for(int element : primes)
    {
        
    }
}
*/




int main()
{ 
    long num = 600851475143;
    int limit = sqrt(num);
    int largest_prime = 0;
    std::deque<int> primes  = eratosthenes(limit);
    for(int i = 0; i<primes.size(); i++)
    {
        int prime = primes[primes.size()-1-i];
        while(num%prime == 0)
        {            
            num = num/prime;
            largest_prime = prime; 

        }
    }
    std::cout << largest_prime << std::endl;
}
