#include <iostream>
#include <string>

bool palindrome(int n)
{
    std::string number = std::to_string(n);
    for(int i = 0; i<number.size(); i++)
    {
        number[i] = number[number.size() - 1 - i];
    }
    int m = std::stoi(number);
    if(m - n == 0)
        return true;
    else
        return false; 
}



int main()
{
    int largest_p = 0;
    int end = 99;
    for(int i = 999; i>end; i--)
    {
        for(int j = i; j>end; j--)
        {
            if(palindrome(j*i) == true and largest_p < j*i)
            {
                largest_p = j*i;
                end = j;
            }
        }
    }
    std::cout << largest_p << std::endl;
}
