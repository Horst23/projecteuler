#include <iostream>

int main(int argc, char const *argv[])
{
    long fib_1 = 1;
    long fib_2 = 0;
    long fib_next = 0;
    long sum = 0;
    long count = 0;


    while(fib_next <= 4000000){
        count++;
        fib_next = fib_1 + fib_2;
        fib_1 = fib_2;
        fib_2 = fib_next;
        if(count == 3){
            sum += fib_2;
            count = 0;
        }

    }
    std::cout << sum;
    return 0;
}
